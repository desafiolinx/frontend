import React from 'react';
import './styles.css';
//utilizando conceito de stateless

const Header = (props) => (
    <header id="main-header">Linx
     <input className="search"
        onKeyUp={(e)=>{props.handleSearch(e.target.value)}}
        id="product"
        placeholder="Buscar produtos"/>
    </header>

);

export default Header;