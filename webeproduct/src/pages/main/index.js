import React, { Component } from 'react';
import Header from '../../components/Header';
import api from '../../services/api';
import './styles.css';

export default class Main extends Component {

    state = {
        products: [],
        productInfo: {},
        search:"", 
        page: 1,
     }

     handleSearch = (text)=>{
        this.setState({ search: text });
        console.log("Search typed:",text);
        this.searchProducts(text);
    }

    searchProducts = async (search) => {
        const response = await api.get(`/?search=${search}`);
        const { docs, ...productInfo } = response.data;
        this.setState({ products: docs, productInfo });
        console.log(response);
    };

    componentDidMount(){
        this.loadProducts();
    }

    loadProducts = async (page = 1) =>{
        const response = await api.get(`/?page=${page}`);

        const { docs, ...productInfo } = response.data;

        this.setState({ products: docs, productInfo});

        console.log(response);
    };

    ViewMore = () => {
        const { page, productInfo} = this.state;

        if(page === productInfo.pages) return ;
        
        const pageNumber = page + 1;
        
        this.loadProducts(pageNumber);
    }

    render(){

        const { products } = this.state;

        return (  
            <>
            <Header handleSearch={this.handleSearch} />
                    <div className="container">
                    <div className="row">
                    {products.map(product => (
                        <div className="col-3" key={product._id}>
                            <div className="card">
                                <div className="row">
                                    <div className="col-12 text-center">
                                        <strong>{product.title}</strong> 
                                    </div>
                                </div>
        
                                <div className="row mt-15">
                                    <div className="col-5">
                                        <img src={product.img} className="img-responsive" alt="item"/> 
                                    </div>
                                    <div className="col-7 text-right">
                                        <p><strike>{`De: R$${product.price}`}</strike></p>
                                        <p><strong>{`Por: R$${product.sPrice}`}</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                            ))}
                    </div>
                </div>
            </>
        );
    }
}